#version 300 es

// INPUT
// - the currently bounded vertex array (VAO) contains a VBO of 3D data (positions)
// - variable is prefixed by "in"
// - its "location index" MUST be the same value when using vertexAttribPointer() and enableVertexAttribArray() during VAO definition
layout(location=1) in vec3 in_position;
layout(location=2) in vec3 in_normal;

// UNIFORM
// - Camera
uniform mat4 uProjectionMatrix;
uniform mat4 uViewMatrix;
// - Model
uniform mat4 uModelMatrix;
// - Normal Matrix
uniform mat3 uNormalMatrix;

// OUPUT
out vec3 frag_position;
out vec3 frag_normal;

void main() {
    frag_position = (uViewMatrix * uModelMatrix * vec4(in_position, 1.0)).xyz;
    frag_normal = normalize(uNormalMatrix * in_normal);

    // MANDATORY : `vec4 gl_Position` in xyzw
    gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(in_position, 1.0);
}