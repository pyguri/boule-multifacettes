"use strict";

let slider_lr, slider_lg, slider_lb;

let slider_kd;
let slider_ks;
let slider_ns;

function init_ui() {
    UserInterface.begin();

    // Light color sliders
        UserInterface.use_field_set('H', "Light color");
        slider_lr = UserInterface.add_slider(null, 0, 100, 100, update_wgl());
        set_widget_color(slider_lr, '#ff0000', '#ff0000');
        slider_lg = UserInterface.add_slider(null, 0, 100, 0, update_wgl());
        set_widget_color(slider_lg, '#00bb00', '#00ee00');
        slider_lb = UserInterface.add_slider(null, 0, 100, 0, update_wgl());
        set_widget_color(slider_lb, '#0000ff', '#0000ff');
        UserInterface.end_use();

    // Light variables
        slider_kd = UserInterface.add_slider('Diffusion', 0, 100, 50, update_wgl());
        slider_ks = UserInterface.add_slider('Scalar reflect', 0, 100, 25, update_wgl());
        slider_ns = UserInterface.add_slider('Roughness', 0, 100, 25, update_wgl());

    UserInterface.end();
}