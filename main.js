"use strict";

function init_wgl() {
    ewgl.continuous_update = true;

    init_ui();
    init_gle();
    load_shader_program("./vertex_shader.vert", "./fragment_shader.frag");
    load_file("./models/flag.json").then(r =>{
        if (r == null)
            console.log("load_file() : nothing to log");
        else
            console.log(r, ...arguments);
    });

}

function draw_wgl() {
    clear_gle();
    update_uniforms();
    draw_meshes();
}

ewgl.launch_3d();