#version 300 es

#define PI 3.1415926535897932384626433832795

precision highp float;


// INPUT
in vec3 frag_position;
in vec3 frag_normal;

// UNIFORM
uniform int uLightCount;
uniform vec3 uLightColor[64];
uniform vec3 uLightPosition[64];
uniform float uDiffusion;
uniform float uSpecular_reflect;
uniform float uRoughness;

// OUPUT
out vec4 out_color;

/**
 * The Fresnel term of the BRDF, here we use the Schlick's approximation.
 *
 * Fresnel equations represent how much light a surface can transmit and reflect,
 * here we are only interested in the reflective factor.
 */
float schlickFresnel(in vec3 lDir, in vec3 hDir){
    return (uSpecular_reflect + (1.0 - uSpecular_reflect) * pow(1.0 - dot(lDir, hDir), 5.0));
}

/**
 * The distribution term of the BRDF, here we use the GGX (or Trowbridge-Reitz') approximation.
 *
 * This term represents how are distributed the facets that reflect light in a given direction.
 * The concentration of these facets is linked to the roughtness of the surface.
 */
float ggxDistribution(in vec3 normal, in vec3 hDir){
    return
        pow(uRoughness, 2.0)
    /
        (PI * pow(pow(dot(normal, hDir), 2.0) * (pow(uRoughness, 2.0) - 1.0) + 1.0, 2.0));
}

/**
 * The geometry term of the BRDF, here we use the Smith function with the Schlick's approximation.
 *
 * This term represent the proportion of facets blocked by other facets when reflecting light in a given direction.
 */
float geomSmith(in vec3 hDir, in vec3 lDir, in vec3 vDir){


    float k = pow(uRoughness + 1.0, 2.0) / 8.0;
    float gl = dot(hDir, lDir) / (dot(hDir, lDir) * (1.0 - k) + k);
    float gv = dot(hDir, vDir) / (dot(hDir, vDir) * (1.0 - k) + k);

    return (gl * gv);
}

/**
 * Bidirectional reflectance distribution function (BRDF) used in physically-based rendering shaders.
 * This function uses a microfacet model where the surface is seen as a set of
 *
 * This function allows us to compute the factor by witch the light must be multiplied in order to render a
 * realistic reflection on the current surface.
 */
float microfacetModel(in vec3 position, in vec3 normal, in int lightID){
    vec3 lightDir = normalize(uLightPosition[lightID] - position);
    vec3 viewDir = normalize(-position);
    vec3 halfDir = normalize(viewDir + lightDir);

    float diffuse = uDiffusion / PI;

    float specular = (
        schlickFresnel(lightDir, halfDir) *
        ggxDistribution(normal, halfDir) *
        geomSmith(halfDir, lightDir, viewDir))
    /
        (4.0 * dot(normal, lightDir) * dot(normal, viewDir));

    return (diffuse + specular);
}

void main() {
    vec3 color = vec3(0.0);
    vec3 light = vec3(0.0);
    vec3 lightDir = vec3(0.0);

    for (int i = 0; i < uLightCount; ++i){
        lightDir = uLightPosition[i] - frag_position;
        light = uLightColor[i];
        color += PI * light * microfacetModel(frag_position, frag_normal, i) * dot(frag_normal, lightDir);
    }

    out_color = vec4(color, 1.0);
}