"use strict";

let program;
let vao;
let nb_indices;

function init_gle() {
    gl.clearColor(0, 0, 0, 1);
    gl.enable(gl.DEPTH_TEST);
    gl.clear(gl.COLOR_BUFFER_BIT);

    program = null;
    vao = null;
    nb_indices = 0;
}

/**
 * Load shader program in the environment
 */
function load_shader_program(vertex_url, frag_url) {
    program = ShaderProgramFromFiles(vertex_url, frag_url);
}

/**
 * Returns computed normals for provided vertices.
 */
function compute_normals(vs, ind) {
    const
        x = 0,
        y = 1,
        z = 2,
        ns = [];

    // For each vertex, initialize normal x, normal y, normal z
    for (let i = 0; i < vs.length; i += 3) {
        ns[i + x] = 0.0;
        ns[i + y] = 0.0;
        ns[i + z] = 0.0;
    }

    // We work on triads of vertices to calculate
    for (let i = 0; i < ind.length; i += 3) {
        // Normals so i = i+3 (i = indices index)
        const v1 = [], v2 = [], normal = [];

        // p2 - p1
        v1[x] = vs[3 * ind[i + 2] + x] - vs[3 * ind[i + 1] + x];
        v1[y] = vs[3 * ind[i + 2] + y] - vs[3 * ind[i + 1] + y];
        v1[z] = vs[3 * ind[i + 2] + z] - vs[3 * ind[i + 1] + z];

        // p0 - p1
        v2[x] = vs[3 * ind[i] + x] - vs[3 * ind[i + 1] + x];
        v2[y] = vs[3 * ind[i] + y] - vs[3 * ind[i + 1] + y];
        v2[z] = vs[3 * ind[i] + z] - vs[3 * ind[i + 1] + z];

        // Cross product by Sarrus Rule
        normal[x] = v1[y] * v2[z] - v1[z] * v2[y];
        normal[y] = v1[z] * v2[x] - v1[x] * v2[z];
        normal[z] = v1[x] * v2[y] - v1[y] * v2[x];

        // Update the normals of that triangle: sum of vectors
        for (let j = 0; j < 3; j++) {
            ns[3 * ind[i + j] + x] = ns[3 * ind[i + j] + x] + normal[x];
            ns[3 * ind[i + j] + y] = ns[3 * ind[i + j] + y] + normal[y];
            ns[3 * ind[i + j] + z] = ns[3 * ind[i + j] + z] + normal[z];
        }
    }

    // Normalize the result.
    // The increment here is because each vertex occurs.
    for (let i = 0; i < vs.length; i += 3) {
        // With an offset of 3 in the array (due to x, y, z contiguous values)
        const nn = [];
        nn[x] = ns[i + x];
        nn[y] = ns[i + y];
        nn[z] = ns[i + z];

        let len = Math.sqrt((nn[x] * nn[x]) + (nn[y] * nn[y]) + (nn[z] * nn[z]));
        if (len === 0) len = 1.0;

        nn[x] = nn[x] / len;
        nn[y] = nn[y] / len;
        nn[z] = nn[z] / len;

        ns[i + x] = nn[x];
        ns[i + y] = nn[y];
        ns[i + z] = nn[z];
    }

    return ns;
}

function load_mesh(mesh) {
    vao = null;
    nb_indices = 0;

    // -------------------------------------------------------------------
    // Part 1: allocate buffers on GPU and send/fill data from CPU
    // -------------------------------------------------------------------
    // [1] - VBO: position buffer
    let data_positions = new Float32Array(mesh.vertices);
    console.log("vertices in data pos", data_positions);
    let vbo_positions = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_positions);
    gl.bufferData(gl.ARRAY_BUFFER, data_positions, gl.STATIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // [2] - VBO: normal buffer
    let data_normals = new Float32Array(compute_normals(mesh.vertices, mesh.indices));
    console.log("normals in data normal", data_normals);
    let vbo_normals = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_normals);
    gl.bufferData(gl.ARRAY_BUFFER, data_normals, gl.STATIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // [3] - EBO: index buffer (for indexed rendering with glDrawElements())
    let ebo_data = new Uint32Array(mesh.indices);
    console.log("indices in data ebo", ebo_data);

    let ebo = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, ebo_data, gl.STATIC_DRAW);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

    // -------------------------------------------------------------------
    // Part 2: Initialize a VAO (vertex array) to hold all previous buffers information
    // -------------------------------------------------------------------
    vao = gl.createVertexArray();
    gl.bindVertexArray(vao);

    // [A]: position buffer
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_positions);
    let vertexAttributeID = 1;
    let dataSize = 3;
    let dataType = gl.FLOAT;
    gl.vertexAttribPointer(vertexAttributeID, dataSize, dataType, false, 0, 0);
    gl.enableVertexAttribArray(vertexAttributeID);

    // [B]: normal buffer
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_normals);
    vertexAttributeID = 2;
    dataSize = 3;
    dataType = gl.FLOAT;
    gl.vertexAttribPointer(vertexAttributeID, dataSize, dataType, false, 0, 0);
    gl.enableVertexAttribArray(vertexAttributeID);

    // [C]: index buffer
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo);

    // Reset GL states
    gl.bindVertexArray(null);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

    // Store the number of indices
    nb_indices = ebo_data.length;
}

/**
 * Fill the vao_list and nbIndices_list with the data from a .json file of a mesh.
 * The file needs the 'vertices' and 'indices' fields
 */
function load_file(file_url) {
    return fetch(file_url)
        .then(file => file.json())
        .then(mesh => load_mesh(mesh))
        .catch((err) => console.error(err, ...arguments));
}

function update_uniforms() {
    program.bind();

    // Camera
    Uniforms.uProjectionMatrix = ewgl.scene_camera.get_projection_matrix();
    Uniforms.uViewMatrix = ewgl.scene_camera.get_view_matrix();

    // Model matrix
    let modelMatrix = Matrix.scale(0.2); // hard-coded scale to be able to see the 3D asset
    Uniforms.uModelMatrix = modelMatrix;

    // Lighting & Shading
    Uniforms.uNormalMatrix = (Matrix.mult(ewgl.scene_camera.get_view_matrix(), modelMatrix)).inverse3transpose();

    // max count : 64
    Uniforms.uLightCount = 3;
    Uniforms.uLightColor = [
        slider_lr.value / 100, slider_lg.value / 100, slider_lb.value / 100,
        slider_lg.value / 100, slider_lr.value / 100, slider_lb.value / 100,
        slider_lg.value / 100, slider_lb.value / 100, slider_lr.value / 100];
    Uniforms.uLightPosition = [
        10.0, 10.0, 0.0,
        -10.0, -10.0, 0.0,
        10.0, 0.0, 10.0];

    Uniforms.uDiffusion = slider_kd.value / 100;
    Uniforms.uSpecular_reflect = slider_ks.value / 100;
    Uniforms.uRoughness = slider_ns.value / 100;
}

function draw_meshes() {
    program.bind();
    let drawMode = gl.TRIANGLES;
    gl.bindVertexArray(vao);
    gl.drawElements(drawMode, nb_indices, gl.UNSIGNED_INT, 0);
}

function clear_gle() {
    gl.bindVertexArray(null);
    gl.useProgram(null);
    gl.clear(gl.COLOR_BUFFER_BIT);
}